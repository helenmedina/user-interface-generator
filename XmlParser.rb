require 'rexml/document'
require "rexml/parseexception"
require_relative 'Table.rb'

class String
  def numeric?
    true if Float self rescue false
  end
end

class XmlParser

	
	def initialize
		@arrayTables = []
	end
	

	def parse(doc)

		print "Parsing table details\n"
		table_name = doc.root.attributes["name"]
		@table = Table.new(table_name)
		#Get the information of the filter section
		SetFilters(@table, doc)
		#Get the information of the output section
		SetOutputs(@table, doc)

	end
	

	def InsertValuesToCalculate(column, value1, value2, operator, table)
		type1 = "column"
		type2 = "column"
		#If one of the operands is not numeric, the generator will create the logic to map the column to the database
		if (value1.numeric?)
			type1 = "number"
		end
		
		if (value2.numeric?)
			type2 = "number"
		end
		table.InsertCalculateDetails ({column: column, value1: value1, type1: type1, operator: operator, value2: value2, type2: type2})
	
	end
	
	def SetFilters(table, doc)

		doc.root.each_element( "filters/filter" ) { |filter|
			column = filter.attributes[ "column" ]
			type = filter.attributes[ "type" ]
			#Get the equivalent type for the c++ language
			type = GetType(type, column)
			option = filter.attributes[ "option" ]
			value = filter.text.strip
			#Get the equivalent option as a string, so the geenrator can use it in the template
			stringFormat = GetStringFormatFromValue(value, column)
			
			if not table.IncludeColumn(column, table.GetColumnFilterDetails)
				table.InsertColumnFilterDetails({columnName: column, columnType: type, valueWithFormat: stringFormat})
			end
			
			table.InsertFilter({column: column, type: type, option: option, value: value, valueWithFormat: stringFormat})
		}
	
	end
	
	def SetOutputs(table, doc)
	
		doc.root.each_element( "outputs/output" ) { |output|
			column = output.attributes[ "column" ]
			type = output.attributes[ "type" ]
			type = GetType(type, column)
			option = output.attributes[ "option" ]
			value = output.text.strip
			
			if not table.IncludeColumn(column, table.GetColumnOutputDetails)
				if(value != nil && !value.empty?)
					table.InsertColumnOutputDetails({columnName: column, columnType: type, option: option})
				end
			end
			
			if (option == "Calculate") 
			
				if (value.index('*') != nil)
					value1 = value.split("*").first
					value2 = value.split("*").last
					InsertValuesToCalculate(column, value1, value2, "*", table)
				elsif (value.index('+') != nil)
					value1 = value.split("+").first
					value2 = value.split("+").last
					InsertValuesToCalculate(column, value1, value2, "+", table)
				elsif (value.index('-') != nil)
					value1 = value.split("-").first
					value2 = value.split("-").last
					InsertValuesToCalculate(column, value1, value2, "-", table)
				elsif (value.index('/') != nil)
					value1 = value.split("/").first
					value2 = value.split("/").last
					InsertValuesToCalculate(column, value1, value2, "/", table)
				end

			end
			table.InsertOutput({column: column, type: type, option: option, value: value})
			
		}
	
	end
	
	def GetTable()
	
		return @table
	
	end
	
	def GetType(type, column)
			print "#{column} #{type}  \n"
			#Get the correspondent data type for the QT creator IDE
			if(type == nil || type.empty?)
				raise Exception , "Type column \"#{column}\" is null."
			else
				case type.downcase
				when "decimal"
					type = 'double'
				when "int"
					type = 'int'
				when "varchar"
					type = 'string'
				when "date"
					type = 'QDateTime'
				else
					raise Exception , "Type \"#{type}\" unknown."
				end
			end
			
		return type
		
	end
	
	def GetStringFormatFromValue(value, column)
		if(value == nil || value.empty?)
			raise Exception , "Type column \"#{column}\" is null."
		else
			case value
			when "Range"
				value = "range"
			when "Date range"
				value = "range"
			when "Start"
				value = "starts"
			when "Contains"
				value = "contains"
			when "=>"
				value = "greater_equal_than"
			when "<="
				value = "less_equal_than"
			when ">"
				value = "greater_than"
			when "<"
				value = "less_than"
			when "=="
				value = "equal_than"
			else
				raise Exception , "Value \"#{value}\" unknown."
			end
		end
	
	
	end
	

end


