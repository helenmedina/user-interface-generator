
class Table

	StFilterOptions = Struct.new(:column, :type, :option, :value, :valueWithFormat)
	StOutputOptions = Struct.new(:column, :type, :option, :value)
	StColumnOutputDetails = Struct.new(:columnName, :columnType, :option)
	StColumnFilterDetails = Struct.new(:columnName, :columnType, :valueWithFormat)
	StCalculateDetails = Struct.new(:column, :value1, :type1, :operator, :value2, :type2)

	attr_accessor :name, :filters, :outputs

	def initialize(name)
		@name = name
		@filters = []
		@outputs = []
		@columnOutputDetails = []
		@columnFilterDetails = []
		@calculateDetails = []
	end
	
	def InsertFilter(fields)

		@filter = StFilterOptions.new(fields[:column], fields[:type], fields[:option], fields[:value], fields[:valueWithFormat]) 
		@filters.push(@filter)

	end
	
	
	def InsertOutput(fields)

		@output = StOutputOptions.new(fields[:column], fields[:type], fields[:option], fields[:value]) 
		@outputs.push(@output)

	end
	
	def InsertColumnOutputDetails(fields)

		@outputDetails = StColumnOutputDetails.new(fields[:columnName], fields[:columnType], fields[:option]) 
		@columnOutputDetails.push(@outputDetails)

	end
	
	def InsertColumnFilterDetails(fields)

		@filterDetails = StColumnFilterDetails.new(fields[:columnName], fields[:columnType], fields[:valueWithFormat]) 
		@columnFilterDetails.push(@filterDetails)

	end
	
	def InsertCalculateDetails(fields)

		@calDetails = StCalculateDetails.new(fields[:column], fields[:value1], fields[:type1], fields[:operator], fields[:value2], fields[:type2]) 
		@calculateDetails.push(@calDetails)

	end
	
	def GetFilters()
	
		return @filters
	
	end
	
	def GetOutputs()

		return @outputs
	
	end
	
	def GetColumnOutputDetails()
	
		return @columnOutputDetails
	
	end
	
	def GetColumnFilterDetails()
	
		return @columnFilterDetails
	
	end
	
	def GetCalculateDetails()
	
		return @calculateDetails
	
	end
	
	def IncludeColumn(column, details)
		#@columnDetails.map { |e| 
		details.map { |e| 
		columnName = e[:columnName] 
		#print "vlues in #{columnName} \n"
		if (columnName == column)
			return true
		end
		}
		
		return false
	
	end
	
	def GetName()
	
		return @name
	
	end
	

end



