#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "helper.h"
#include <QtSql>
#include <QComboBox>
#include <QMessageBox>
#include <QLayout>
#include <QLayoutItem>
#include <QMultiMap>
#include<QToolTip>
#include<vector>

/***************************************************************/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow)
/***************************************************************/
{
    Init();
    SetUpFilterDetails();

    //Set up connections
    connect(ui->retrieveButton, SIGNAL(clicked()), SLOT(RetrieveColumns()));
    connect(ui->addFilterButton, SIGNAL(clicked()), SLOT(AddFilter()));
    connect(ui->deleteFilterButton, SIGNAL(clicked()), SLOT(deleteFilter()));
    connect(ui->saveFilterButton, SIGNAL(clicked()), SLOT(SaveFilters()));
}

/***************************************************************/
void MainWindow::Init()
/***************************************************************/
{
    ui->setupUi(this);
    this->setWindowTitle("Filter Configuration");
    //Set up layout
    QWidget *window = new QWidget();
    window->setLayout(ui->verticalLayout);
    setCentralWidget(window);

    //Set init values
    m_columnMap.clear();
    m_filterIDMap.clear();
    m_columnList.clear();
    m_dateFilterList.clear();
    m_stringFilterList.clear();
    m_numberFilterList.clear();
    m_columnTableRef = nullptr;
}

/***************************************************************/
void MainWindow::SetUpFilterDetails()
/***************************************************************/
{
    ui->addFilterButton->setDisabled(true);
    ui->deleteFilterButton->setDisabled(true);
    ui->saveFilterButton->setDisabled(true);
    ui->textEdit->setDisabled(true);
    m_iCounter = 0;


    //Creates the filters list for a data type
    helper.BuildDateFilterOptions(m_dateFilterList);
    helper.BuildStringFilterOptions(m_stringFilterList);
    helper.BuildNumberFilterOptions(m_numberFilterList);
}

/***************************************************************/
void MainWindow::RetrieveColumns()
/***************************************************************/
{
    QSqlDatabase db;
    m_tableName = ui->lineEdit->text();

    if (!OpenConnection(db))
        qDebug() << db.lastError();
    else
    {
        qDebug() << "Connected";

        //exec the query to retrieve the columns
        QString query = "SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '";
        query.append(m_tableName);
        query.append("'");

        QSqlQuery sqlQuery;
        sqlQuery.exec(query);
        QString columnName;
        QString columnType;

        while (sqlQuery.next()) {

            //Get table's column names and the data types
            columnName = sqlQuery.value(0).toString();
            columnType = sqlQuery.value(1).toString();

            if(m_columnMap.find(columnName) == m_columnMap.end())
                m_columnMap.insert(columnName, columnType);
        }

        db.close();
    }

    EnableButtons();
    BuildNameColumnList();
    PopulateOutputParameters();
}

/**************************************/
void MainWindow::EnableButtons()
/*************************************/
{
    if(!m_columnMap.isEmpty())
    {
        ui->addFilterButton->setEnabled(true);
        ui->deleteFilterButton->setEnabled(true);
        ui->textEdit->setEnabled(true);
        ui->textEdit->setToolTip("Configure the output options:\n Use \"Format\" option to apply the format to the column. The separator is comma.\n "
                                                "Use \"Calculate\" option to apply an operation between columns or other operands ");
    }
}

/************************************************/
void MainWindow::PopulateOutputParameters()
/************************************************/
{
    QString rows;
    int iOrder = 0;
    for(auto column : m_columnMap.toStdMap())
    {
        iOrder++;
        QString columnName = column.first;
        QString columnType = column.second;
        QStringList outputFormatList = GetOutputFormatList(columnType);
        rows += GetOutputRow(iOrder, columnName, outputFormatList) + "<br/>";
    }
    ui->textEdit-> setText(rows);
}

/**************************************************************/
QStringList MainWindow::GetOutputFormatList(QString type)
/**************************************************************/
{
    QStringList formatList;
    if (Helper::IsDate(type))
    {
        formatList.append("yyyy/mm/dd");
        formatList.append("yyyy-mm-dd");
    }
    else if(Helper::IsString(type))
    {
        formatList.append("upper case");
        formatList.append("lower case");
    }
    else
    {
		formatList.append("default");
	}

    return formatList;
}

/*****************************************************************************************************/
QString MainWindow::GetOutputRow(int iOrder, QString columnName, QStringList outputFormatList)
/****************************************************************************************************/
{
    QString strColumn = " Column: ";
    QString columnBold = "<html><b>" + strColumn + "</b></html>";
    QString strFormat = " Format: ";
    QString outputFormat = "<html><b>" + strFormat + "</b></html>";

    QString outputColumn = QString::number(iOrder) + columnBold + columnName;

    if(!outputFormatList.isEmpty())
    {
        bool bFirstIteration = true;
        for (QStringList::iterator it = outputFormatList.begin(); it != outputFormatList.end(); ++it)
        {
            if(bFirstIteration)
            {
                outputFormat += *it;
                bFirstIteration = false;
            }
            else
             outputFormat += ", " + *it;
        }
    }

    return outputColumn + "; " +outputFormat;
}

/***************************************************************/
bool MainWindow::OpenConnection(QSqlDatabase &db)
/***************************************************************/
{
    bool bOpened = true;
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setDatabaseName("restaurant");
    db.setUserName("hmedina");
    db.setPassword("mysql");

    if (!db.open())
        bOpened = false;

    return bOpened;
}

/***************************************************************/
void MainWindow::BuildNameColumnList()
/***************************************************************/
{
    for(auto column : m_columnMap.toStdMap())
    {
        //if item not found in list, inserted to the list
        if (m_columnList.indexOf(column.first) == -1)
          m_columnList.append(column.first);
    }
}

/***************************************************************/
void MainWindow::AddFilter()
/***************************************************************/
{ 
    QComboBox  *columnTable = new QComboBox(this);
    columnTable->setObjectName(QString::number(m_iCounter++));
    columnTable->setMinimumSize(85,27);
    ui->verticalLayout->addWidget(columnTable);

    //Add the column name list to the combobox
    columnTable->addItems(m_columnList);

    //Call setFilter() if item is selected in the combobox
    connect(columnTable, SIGNAL( currentIndexChanged(int)), this, SLOT(SetFilter(int)));
}

/***************************************************************/
void MainWindow::SetFilter(int index)
/***************************************************************/
{
    QComboBox* columnTable = qobject_cast<QComboBox*>(sender());
    QString filterID = columnTable->objectName();
    columnTable->setFocus();
    m_columnTableRef = columnTable;

     //Only creates the filter option combobox if it wasn't in this layout and it is added to the map
    if(m_filterIDMap.find(filterID) == m_filterIDMap.end())
        CreateNewFilter(filterID, columnTable);

    //Get the second combobox for this layout
    QComboBox  *columnFilter;
    auto itFilter = m_filterIDMap.find(columnTable->objectName());
    if(itFilter != m_filterIDMap.end()){
       sFilter filter = itFilter.value();
       columnFilter = filter.filterOption;
    }

    //Get the filter options defined for the column and added to the second combobox
   QString columnType = GetColumnTypeSelected(index);
   QStringList &filterOptionsList = GetFilterOptionsForColumnSelected(columnType);
   columnFilter->clear();
   columnFilter->addItems(filterOptionsList);
   ui->saveFilterButton->setEnabled(true);

}

/***************************************************************/
QString MainWindow:: GetColumnTypeSelected(int index)
/***************************************************************/
{
    QString columnName = m_columnList.at(index);

    return GetColumnType(columnName);
}

/***************************************************************/
QString MainWindow:: GetColumnType(QString columnName)
/***************************************************************/
{
    QString columnType = "";
    auto it =m_columnMap.find(columnName);

    if(it != m_columnMap.end()){
        columnType = it.value();
    }

    return columnType;
}

/*******************************************************************************/
void MainWindow::CreateNewFilter(QString filterID, QComboBox* columnTable)
/*******************************************************************************/
{
    QComboBox  *columnFilter = new QComboBox(columnTable);
    columnFilter->setMinimumSize(85,27);

    QHBoxLayout*  hBoxFilter = new QHBoxLayout(this);
    hBoxFilter->addWidget(columnTable);
    hBoxFilter->addWidget(columnFilter);
    ui->verticalLayout->addLayout(hBoxFilter);

    sFilter filterRow;
    filterRow.columnName = columnTable;
    filterRow.columnType = GetColumnType(filterRow.columnName->currentText());
    filterRow.filterOption = columnFilter;
    m_filterIDMap.insert(filterID, filterRow);

}

/************************************************************************************/
QStringList& MainWindow::GetFilterOptionsForColumnSelected(QString columnType)
/************************************************************************************/
{
    QStringList *filterList;

    if (Helper::IsDate(columnType))
        filterList = &m_dateFilterList;
    else if (Helper::IsString(columnType))
        filterList = &m_stringFilterList;
    else if (Helper::IsNumber(columnType))
        filterList = &m_numberFilterList;
    else
    {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","An error has occured !");
    }

    return *filterList;
}

/*********************************************************************/
void MainWindow::deleteFilter()
/*********************************************************************/
{
    if (m_columnTableRef !=nullptr)
    {
        auto itFilter = m_filterIDMap.find(m_columnTableRef->objectName());

        if(itFilter != m_filterIDMap.end())
        {
            sFilter filter = itFilter.value();
            QComboBox  *columnFilter = filter.filterOption;
            delete columnFilter;
            columnFilter = nullptr;
        }

        delete m_columnTableRef;
        m_columnTableRef = nullptr;
    }
}

/*********************************************************************/
void MainWindow::SaveFilters()
/*********************************************************************/
{
    QMultiMap<QString, stColumnDetails> filters = GetFiltersSelected();
    QMultiMap<QString, stColumnDetails> outputs = GetOutputsSelected();

    WriteXMLFile(filters, outputs);

}

/*********************************************************************/
QMultiMap<QString, stColumnDetails> MainWindow::GetOutputsSelected()
/*********************************************************************/
{
    QString strOutputParameters = ui->textEdit->toPlainText();
    QMultiMap<QString, stColumnDetails> output;
    QStringList lines = strOutputParameters.split("\n");

    for (QStringList::iterator it = lines.begin(); it != lines.end(); ++it)
    {
        GetParametersFromLine(output, *it);
    }

    return output;

}

/*********************************************************************/
QMultiMap<QString, stColumnDetails> MainWindow::GetFiltersSelected()
/*********************************************************************/
{
    QMultiMap<QString, stColumnDetails> filters;
    for(auto row : m_filterIDMap.toStdMap())
    {
        sFilter filterRow = row.second;
        stColumnDetails columnDetails;
        columnDetails.optionColumn = filterRow.filterOption->currentText();
        columnDetails.typeColumn = GetColumnType(filterRow.columnName->currentText());//filterRow.columnType;

        filters.insert(filterRow.columnName->currentText(), columnDetails);
    }

    return filters;
}

/*********************************************************************/
void MainWindow::GetParametersFromLine(QMultiMap<QString, stColumnDetails>& output, QString line)
/*********************************************************************/
{
   QStringList columnString = line.split(';').first().split(':');
   QStringList formatString = line.split(';').last() .split(':');
   QString columnName = columnString.last();
   columnName = columnName.trimmed();

    if (formatString.last() == "")
        InsertOutputDetails(output, columnName, "");
    else
    {
        QString method = line.split(';').last().split(':').first();
        if (method.indexOf("Calculate") != -1)
        {
            QString columnOperation = formatString.last();
           if( IfOperationIsValid(columnOperation) )
               InsertOutputDetails(output, columnName, columnOperation);
                   // output.insert(columnString.last(), columnOperation);
        }
        else if (method.indexOf("Format") != -1)
        {
            QStringList options = formatString.last().split(',');
            for (QStringList::iterator it = options.begin(); it != options.end(); ++it)
                 InsertOutputDetails(output, columnName, *it);
        }

    }

}

void MainWindow::InsertOutputDetails(QMultiMap<QString, stColumnDetails>& output, QString columnName, QString option)
{
    stColumnDetails columnDetails;
    columnDetails.optionColumn = option;
    columnDetails.typeColumn = GetColumnType(columnName.trimmed());
    //if type is not founded it is calculation format so the type is decimal
    if (columnDetails.typeColumn == "")
    {
        columnDetails.typeColumn = "decimal";
        columnDetails.columnCalculate = "Calculate";
        //Find the column in the expression to save it in the xml
        /*for(auto column : m_columnMap.toStdMap())
        {
            QString columnName = column.first;
            if (option.toStdString().find(columnName.toStdString()) != std::string::npos)
                    columnDetails.columnCalculate =  columnName;
        }*/
    }
    output.insert(columnName,columnDetails);

}

bool MainWindow::IfOperationIsValid(QString columnOperation)
{
    bool bFound = false;
    QString operators[4] {"*","+", "-", "/"};
    QString firstOperand ;
    QString secondOperand;

    for (int i =0; i < 4 && !bFound; i++ )
    {
        firstOperand = columnOperation.split(operators[i]).first();
        secondOperand = columnOperation.split(operators[i]).last();
        if (firstOperand != secondOperand)
        {
              bFound = true;
        }
    }
    if (!bFound)
    {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Operantion not valid");
    }
    else
    {
        CheckOperand(firstOperand);
        CheckOperand(secondOperand);
    }

    return bFound;
}

void MainWindow::CheckOperand(QString operand)
{
    bool ok;
    //Only the operation is valid if it is column table or a number
    operand.toInt(&ok, 10);
    //If it is not a number, it checks if it is a column
    if(!ok)
    {
        auto it = m_columnMap.find(operand);
        if(it != m_columnMap.end())
        {
            QString columnType = it.value();
               if (!Helper::IsNumber(columnType))
               {
                   QMessageBox messageBox;
                   messageBox.critical(0,"Error", "Type column is not a number", columnType);
               }

        }
        else
        {
            //QMessageBox messageBox;
            //messageBox.critical(0,"Error", "Operand not valid", operand);
        }
    }
}



/***********************************************************************************************************************/
void MainWindow::WriteXMLFile(QMultiMap<QString, stColumnDetails>&filters, QMultiMap<QString, stColumnDetails> &outputs) const
/***********************************************************************************************************************/
{
    XMLHandle xml;
    xml.SetFilterNode(filters);
    xml.SetOutputQueryNode(outputs);
    xml.WriteXMLFile(m_tableName);
}

/***************************************************************/
MainWindow::~MainWindow()
/***************************************************************/
{
    delete ui;
}
