#include "xmlhandle.h"
#include<QXmlStreamWriter>
#include<QFile>
#include <QMessageBox>


XMLHandle::XMLHandle()
{

}

void XMLHandle::SetFilterNode(const QMultiMap<QString, stColumnDetails> &filters)
{
    m_filterNode = filters;
}

void XMLHandle::SetOutputQueryNode( const QMultiMap<QString, stColumnDetails> &outputs)
{
    m_outputNode = outputs;
}

void XMLHandle::WriteXMLFile(QString tableName)
{
    QXmlStreamWriter xmlWriter;
        QFile file("../output.xml");

        if (!file.open(QIODevice::WriteOnly))
        {
            QMessageBox::warning(0, "Error!", "Error opening file");
        }
        else
        {
            xmlWriter.setDevice(&file);
            xmlWriter.writeStartDocument();
            xmlWriter.writeStartElement("table");
            xmlWriter.writeAttribute("name", tableName);
            AddFiltersNode(xmlWriter);
            AddOutputsNode(xmlWriter);

            xmlWriter.writeEndElement();
            xmlWriter.writeEndDocument();
       }

}

void XMLHandle::AddFiltersNode(QXmlStreamWriter &xmlWriter)
{
    xmlWriter.writeStartElement("filters");

    AddNodes("filter", m_filterNode, xmlWriter);

    xmlWriter.writeEndElement();
}
void XMLHandle::AddOutputsNode(QXmlStreamWriter &xmlWriter)
{
    xmlWriter.writeStartElement("outputs");

    AddNodes("output", m_outputNode, xmlWriter);

    xmlWriter.writeEndElement();
}

void XMLHandle::AddNodes(const QString nodeName, QMultiMap<QString, QString>& nodes, QXmlStreamWriter &xmlWriter)
{
    QMultiMap<QString, QString>::iterator it = nodes.begin();
    while (it != nodes.end())
    {
        xmlWriter.writeStartElement(nodeName);
        xmlWriter.writeAttribute("column", it.key());
        xmlWriter.writeAttribute("option", "output");
        xmlWriter.writeCharacters (it.value());
        xmlWriter.writeEndElement();
        ++it;
    }
}

void XMLHandle::AddNodes(const QString nodeName, QMultiMap<QString, stColumnDetails>& nodes, QXmlStreamWriter &xmlWriter)
{
    QMultiMap<QString, stColumnDetails>::iterator it = nodes.begin();
    while (it != nodes.end())
    {
        stColumnDetails stColumn = it.value();

        xmlWriter.writeStartElement(nodeName);
        xmlWriter.writeAttribute("column", it.key());
        xmlWriter.writeAttribute("type", stColumn.typeColumn);
        xmlWriter.writeAttribute("option", stColumn.columnCalculate);
        xmlWriter.writeCharacters (stColumn.optionColumn);
        xmlWriter.writeEndElement();
        ++it;
    }
}
