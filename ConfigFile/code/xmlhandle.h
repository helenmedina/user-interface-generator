#ifndef XMLHANDLE_H
#define XMLHANDLE_H
#include <QMultiMap>

class QString;
class QXmlStreamWriter;

struct stColumnDetails
{
    QString optionColumn;
    QString typeColumn;
    QString columnCalculate;
};


class XMLHandle
{
public:
    XMLHandle();
    void SetFilterNode(const QMultiMap<QString, stColumnDetails>&);
    void SetOutputQueryNode( const QMultiMap<QString, stColumnDetails>&);
    void AddFiltersNode(QXmlStreamWriter &xmlWriter);
    void AddOutputsNode(QXmlStreamWriter &xmlWriter);
    void AddNodes(const QString nodeName, QMultiMap<QString, QString>& nodes, QXmlStreamWriter &xmlWriter);
    void AddNodes(const QString nodeName, QMultiMap<QString, stColumnDetails>& nodes, QXmlStreamWriter &xmlWriter);
    void WriteXMLFile(QString);

private:
    QMultiMap<QString, stColumnDetails> m_filterNode;
    QMultiMap<QString, stColumnDetails> m_outputNode;
};

#endif // XMLHANDLE_H
