#-------------------------------------------------
#
# Project created by QtCreator 2017-02-12T18:26:38
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = UserConfigure
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    xmlhandle.cpp \
    helper.cpp

HEADERS  += mainwindow.h \
    xmlhandle.h \
    helper.h

FORMS    += mainwindow.ui

CONFIG += c++11
INCLUDEPATH += -I /usr/include/mysql
LIBS +=-L/usr/lib/x86_64-linux-gnu/qt5/plugins/sqldrivers -lqsqlmysql
