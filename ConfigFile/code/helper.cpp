#include "helper.h"
#include <QStringList>

Helper::Helper()
{

}


/*********************************************************************/
void Helper::BuildNumberFilterOptions(QStringList &numberFilterList)
/*********************************************************************/
{
    numberFilterList.append("Range");
    numberFilterList.append("=>");
    numberFilterList.append("<=");
    numberFilterList.append(">");
    numberFilterList.append("<");
    numberFilterList.append("==");
}

/*********************************************************************/
void Helper::BuildStringFilterOptions(QStringList &stringFilterList)
/*********************************************************************/
{
    stringFilterList.append("Start");
    stringFilterList.append("Contains");
    stringFilterList.append("==");
}

/*********************************************************************/
void Helper::BuildDateFilterOptions(QStringList &dateFilterList)
/*********************************************************************/
{
    dateFilterList.append("Date range");
    dateFilterList.append(">");
    dateFilterList.append("=>");
    dateFilterList.append("<");
    dateFilterList.append("<=");
}

/*********************************************************************/
bool Helper::IsDate(const QString &type)
/*********************************************************************/
{
  return (type == "date" || type == "datetime" || type == "time");
}

/*********************************************************************/
bool Helper::IsString(const QString &type)
/*********************************************************************/
{
    return (type == "char" || type == "varchar" || type == "text");
}

/*********************************************************************/
bool Helper::IsNumber(const QString &type)
/*********************************************************************/
{
     return (type == "int" || type == "tinyint" || type == "bigint" ||  type == "double" || type == "decimal");
}
