#ifndef HELPER_H
#define HELPER_H

class QStringList;
class QString;

class Helper
{

public:
    Helper();
    void BuildNumberFilterOptions(QStringList&);
    void BuildStringFilterOptions(QStringList&);
    void BuildDateFilterOptions(QStringList&);
    static bool IsDate(const QString&);
    static bool IsString(const QString&);
    static bool IsNumber(const QString&);

};

#endif // HELPER_H
