#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <helper.h>
#include "xmlhandle.h"

class QString;
class QHBoxLayout;
class QComboBox;
class QSqlDatabase;
class QXmlStreamWriter;
class Helper;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    struct sFilter
    {
        QComboBox* columnName;
        QComboBox* filterOption;
        QString columnType;
    };

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void Init();
    void EnabledButtons();
    void PopulateOutputParameters();
    void SetUpFilterDetails();
    bool OpenConnection(QSqlDatabase &db);
    void BuildNameColumnList();
    QString GetColumnTypeSelected(int index);
    QStringList& GetFilterOptionsForColumnSelected(QString);
    void CreateNewFilter(QString, QComboBox*);
    QString GetOutputRow(int, QString columnName, QStringList outputFormatList);
    QStringList GetOutputFormatList(QString type);
    void EnableButtons();
    QMultiMap<QString, stColumnDetails> GetFiltersSelected();
    void WriteXMLFile(QMultiMap<QString, stColumnDetails>&, QMultiMap<QString, stColumnDetails>&) const;
    QMultiMap<QString, stColumnDetails> GetOutputsSelected();
    void GetParametersFromLine(QMultiMap<QString, stColumnDetails>& output, QString line);
    void InsertOutputDetails(QMultiMap<QString, stColumnDetails>& output, QString columnName, QString option);
    bool IfOperationIsValid(QString);
    void CheckOperand(QString );
    QString GetColumnType(QString columnName);

public slots:
    void RetrieveColumns();
    void AddFilter();
    void deleteFilter();
    void SetFilter(int);
    void SaveFilters();

protected:
    //void keyPressEvent(QKeyEvent * event);
private:
    Ui::MainWindow *ui;
    QMap<QString, QString> m_columnMap;
    QMap<QString, sFilter> m_filterIDMap;
    QStringList m_columnList;
    QStringList m_dateFilterList;
    QStringList m_stringFilterList;
    QStringList m_numberFilterList;
    QString m_tableName;
    QComboBox* m_columnTableRef;
    Helper helper;
    int m_iCounter;


};

#endif // MAINWINDOW_H
