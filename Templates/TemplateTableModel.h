#ifndef   <%= class_name.upcase %>_TABLE_MODEL_H
#define <%= class_name.upcase %>_TABLE_MODEL_H
#include <string>
#include<QDateTime>
#include "shared.h"

using namespace std;

class <%= class_name.capitalize %>TableModel
{
	
public:
	<%= class_name.capitalize %>TableModel();
	string get_table_name();
    <% columnOutputDetails.each { |column| %>

	<% } %>

<%-columnOutputDetails.each { |column| 
if (column[:columnType] == "QDateTime")-%>
	void set_<%= column[:columnName] %> (string); 
	string get_<%= column[:columnName] %>();
<%-else -%>
	void set_<%= column[:columnName] %> (<%= column[:columnType] %>); 
	<%= column[:columnType] %> get_<%= column[:columnName] %>();
<%-end } -%>



	<%-columnFilterDetails.each { |column| 
	if (column[:columnType] == "string")-%>
	string get_filter_for_<%= column[:columnName] %> (eFilters filterType, string str); 
	<%-else -%>
	string get_filter_for_<%= column[:columnName] %> (eFilters filterType, string value1, string value2 = ""); 
	<%-end } -%>
	void getFilter(eFilters filterType, string &filter, string column, string value1, string value2 ="");


private:

	string m_tableName;
	string m_query;
<%-columnOutputDetails.each { |column| 
if (column[:columnType] == "QDateTime")-%>
	string m_<%= column[:columnName] %>;
<%-else -%>
	<%= column[:columnType] %> m_<%= column[:columnName] %>;
<%-end } -%>

};

#endif // <%= class_name.upcase %>_TABLE_MODEL_H
