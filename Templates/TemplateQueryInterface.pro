#-------------------------------------------------
#
# Project created by QtCreator 2017-02-26T18:45:52
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QueryInterface
TEMPLATE = app


SOURCES += main.cpp\
    controller.cpp \
    tableview.cpp \
    <%= class_name.capitalize %>TableLogic.cpp \
    <%= class_name.capitalize %>TableModel.cpp

HEADERS  += \
    controller.h \
    shared.h \
    tableview.h \
    <%= class_name.capitalize %>TableLogic.h \
    <%= class_name.capitalize %>TableModel.h

FORMS    += mainwindow.ui

CONFIG += c++11
