#include <QFormLayout>
#include<QComboBox>
#include<QTableWidget>
#include<map>
#include "tableview.h"
#include "ui_mainwindow.h"

/******************************************************************************************/
TableView::TableView(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
/******************************************************************************************/
{
    //Initiate values
    ui->setupUi(this);
    ui->lineEdit->setEnabled(false);
    ui->lineEdit->setText("<%= class_name %>");
    ui->retrieveButton->setEnabled(true);

    connect(ui->retrieveButton, SIGNAL(clicked()), SLOT(Retrieve()));
}

/***************************************/
void TableView::Retrieve()
/***************************************/
{
    SetLayouts();
    SetUpOuputOptions(m_controller.GetOutputs());
    SetUpFilterOptions(m_controller.GetFilters());
    AddExecuteButton();
}

/***************************************/
void TableView::SetLayouts()
/***************************************/
{
    ui->verticalLayout->addLayout(ui->horizontalLayout);
    QWidget *window = new QWidget();
    window->setLayout(ui->verticalLayout);
    setCentralWidget(window);
}

/***************************************/
void TableView::AddExecuteButton()
/***************************************/
{
    QPushButton *executeQuery = new QPushButton(this);
    executeQuery->setText("Execute Query");
    connect(executeQuery, SIGNAL(clicked()), SLOT(Execute()));

    ui->verticalLayout->addWidget(executeQuery);
}

/***********************************************************/
void TableView::SetUpFilterOptions( MultiStringMap &filters)
/***********************************************************/
{
    //Add label ->Filters
     QLabel *filterLabel= new QLabel("Filters");
     QFont f("Arial", 11, QFont::Bold);
     filterLabel->setFont(f);

     //Add filter option row layout to the main layout
     ui->verticalLayout->addWidget(filterLabel);
     ui->verticalLayout->addWidget(ui->filterLine);

     BuildFiltersToScreen(filters);
}

/***************************************************************/
void TableView::SetUpOuputOptions( MultiStringMap &outputs)
/***************************************************************/
{
    //Add label outputs
    QLabel *outputLabel= new QLabel("Outputs");
    QFont f("Arial", 11, QFont::Bold);
    outputLabel->setFont (f);
    //Add output option layout to the main layout
    ui->verticalLayout->addWidget(outputLabel);
    ui->verticalLayout->addWidget(ui->outputLine);

    BuildOutputsToScreen(outputs);
}

/*********************************************************************/
void TableView::BuildOutputsToScreen(MultiStringMap &outputMap)
/*********************************************************************/
{
    MultiStringMap::iterator outputIter;

    for (outputIter = outputMap.begin();  outputIter != outputMap.end(); )
    {
        QString valueColumn = (*outputIter).second.filterColumn;
        if (!valueColumn.isEmpty() && !valueColumn.isNull() )
        {
            //Add label with column name
            string key = (*outputIter).first;
            QLabel *columnName = AddLabel(key);

            //Add the list with the different output options for the column name
            QStringList strOptionList;
            BuildOptionList(outputMap,  outputIter, strOptionList, key);
            QComboBox *optionList = AddOptionList(strOptionList);

            //Set up layouts
            QHBoxLayout *hLayout = new QHBoxLayout;
            hLayout->addWidget(columnName);
            hLayout->addWidget(optionList);
            ui->verticalLayout->addLayout(hLayout);

            //Insert the output elements in the controller
            st_ouputElements stOutput;
            stOutput.columnName = columnName;
            stOutput.optionList = optionList;
            m_controller.m_outputElements.push_back(stOutput);
        }
        else
        {
            ++outputIter;
        }

    }
}

/*******************************************************************/
void TableView::BuildFiltersToScreen(MultiStringMap &filtersMap )
/*******************************************************************/
{
    MultiStringMap::iterator filterIter;
    int iCounter = 0;

    for (filterIter = filtersMap.begin();  filterIter != filtersMap.end(); )
    {
        stColumnDetails columnDetails = (*filterIter).second;
        //Add label with column name
        string key = (*filterIter).first;
        QLabel *columnName = AddLabel(key);

        //Add list with the different filter options for the column
        QStringList strOptionList;
        BuildOptionList(filtersMap,  filterIter, strOptionList, key);        
        QComboBox *optionList = AddOptionList(strOptionList);
        optionList->setObjectName(QString::number(iCounter++));

        //Add line edit to insert the value for the filter condition
        QLineEdit *filter = new QLineEdit();
        filter->setDisabled(true);

        QLineEdit *betweenFilter = new QLineEdit();
        betweenFilter->setVisible(false);

        //Add the conjunction to join the filters
        QComboBox *conjunction = AddConjunctionList();
        conjunction->setDisabled(true);

        //Set up layouts for the filter tab
        QHBoxLayout *hLayout = new QHBoxLayout;
        hLayout->addWidget(columnName);
        hLayout->addWidget(optionList);
        hLayout->addWidget(filter);
        hLayout->addWidget(betweenFilter);
        hLayout->addWidget(conjunction);
        ui->verticalLayout->addLayout(hLayout);

        connect(optionList, SIGNAL(currentIndexChanged(int)), this, SLOT(HandleFilter(int)));

        //Insert filter elements in the controller
        stFilter = new st_filterElements();
        stFilter->columnName = columnName;
        stFilter->optionList = optionList;
        stFilter->input = filter;
        stFilter->betweenInput = betweenFilter;
        stFilter->conjunction = conjunction;
        stFilter->type = columnDetails.typeColumn;

        m_controller.m_filterElements.push_back(stFilter);

    }
}

/************************************************************************************************************************************/
void TableView::BuildOptionList(MultiStringMap &map, MultiStringMap::iterator &Iter, QStringList &strOptionList, const string &key)
/************************************************************************************************************************************/
{
    MultiStringMap::iterator rangeIter;
    pair<MultiStringMap::iterator, MultiStringMap::iterator> keyRange = map.equal_range(key);

    for (rangeIter = keyRange.first;  rangeIter != keyRange.second;  ++rangeIter)
    {
        stColumnDetails columnDetails = (*rangeIter).second;
        QString option = columnDetails.filterColumn;

        if (strOptionList.indexOf(option) == -1)
        {
            strOptionList.append(option);
        }
    }

    Iter = rangeIter;
}

/********************************************/
QLabel* TableView::AddLabel(string name)
/*******************************************/
{
    QLabel *label = new QLabel(QString::fromStdString(name));
    label->setMinimumSize(95,27);

    return label;
}

/*******************************************************************/
QComboBox* TableView::AddOptionList(QStringList &strOptionList)
/*******************************************************************/
{
    QComboBox  *optionList = new QComboBox(this);
    optionList->setMinimumSize(95,27);

    //Add the column name list to the combobox
    optionList->addItems(strOptionList);

    return optionList;
}

/***********************************************/
QComboBox* TableView::AddConjunctionList()
/***********************************************/
{
    QComboBox  *joinQuery = new QComboBox(this);
    joinQuery->setMaximumSize(60,27);

    QStringList strOptionList;
    strOptionList.append("AND");
    strOptionList.append("OR");
    strOptionList.append("_");

    //Add the column name list to the combobox
    joinQuery->addItems(strOptionList);

    return joinQuery;
}

/******************************************/
void TableView::HandleFilter(int index)
/******************************************/
{
    QComboBox* filterOptionSender = qobject_cast<QComboBox*>(sender());

    for (std::vector<st_filterElements>::size_type i = 0; i< m_controller.m_filterElements.size(); i++)
    {

        if (m_controller.m_filterElements[i]->optionList->objectName() == filterOptionSender->objectName())
        {

            QString currentText = filterOptionSender->itemText(index);
            //If the filter  is None, then filter options are disabled
            if (currentText == "None")
            {
                m_controller.m_filterElements[i]->input->setDisabled(true);
                m_controller.m_filterElements[i]->conjunction->setDisabled(true);
                m_controller.m_filterElements[i]->betweenInput ->setDisabled(true);
                m_controller.DisableFilterWhenNoneSelected(m_controller.m_filterElements[i]->columnName->text());

            }
            else if(currentText == "Range" || currentText == "Date range")
            {
                m_controller.m_filterElements[i]->input->setDisabled(false);
                m_controller.m_filterElements[i]->conjunction->setDisabled(false);
                m_controller.m_filterElements[i]->betweenInput ->setDisabled(false);
                m_controller.m_filterElements[i]->betweenInput->setVisible(true);
            }
            else
            {
                m_controller.m_filterElements[i]->input->setDisabled(false);
                m_controller.m_filterElements[i]->conjunction->setDisabled(false);
            }
         }

    }

}


/**************************/
void TableView::Execute()
/**************************/
{
    QTableWidget* table = m_controller.execute();
    ui->verticalLayout->addWidget(table);
}

/**************************/
TableView::~TableView()
/**************************/
{
    delete ui;
}
