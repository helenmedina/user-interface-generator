#include "<%= class_name.capitalize %>TableModel.h"

<%= class_name.capitalize %>TableModel::<%= class_name.capitalize %>TableModel()
{
	m_tableName = "<%= class_name%>";
}

string <%= class_name.capitalize %>TableModel::get_table_name()
{
	return m_tableName;
}

	<%-columnOutputDetails.each { |column| 
	if (column[:columnType] == "string")-%>
void <%= class_name.capitalize%>TableModel::set_<%= column[:columnName] %> (<%= column[:columnType] %> str)
{
	m_<%= column[:columnName] %> = str;
}
	<%-elsif (column[:columnType] == "QDateTime")  -%>
void <%= class_name.capitalize%>TableModel::set_<%= column[:columnName] %> (string tm)
{
	m_<%= column[:columnName] %> = tm;
}
	<%-else -%>
void <%= class_name.capitalize%>TableModel::set_<%= column[:columnName] %> (<%= column[:columnType] %> id)
{
	m_<%= column[:columnName] %> = id;
}
	<%-end } -%>
	
	
	
	
	<%-columnOutputDetails.each { |column| 
	if (column[:columnType] == "QDateTime")-%>
string <%= class_name.capitalize%>TableModel::get_<%= column[:columnName] %> ()
{
	return m_<%= column[:columnName]%>;
}

	<%-else -%>
<%= column[:columnType] %> <%= class_name.capitalize%>TableModel::get_<%= column[:columnName] %> ()
{
	return m_<%= column[:columnName]%>;
}
	<%-end } -%>


<%-columnFilterDetails.each { |column| 
if (column[:columnType] == "string")-%>
string <%= class_name.capitalize%>TableModel::get_filter_for_<%= column[:columnName] %> (eFilters filterType, string str)
{
	string column = "<%= column[:columnName] %>";
	string filter = column;
	getFilter(filterType, filter, column, str);
	return filter;
} 
<%-else -%>
string <%= class_name.capitalize%>TableModel::get_filter_for_<%= column[:columnName] %> (eFilters filterType, string value1, string value2 /*=""*/)
{
	string column = "<%= column[:columnName] %>";
	string filter = column;
	getFilter(filterType, filter, column, value1, value2);
	return filter;
}
<%-end } -%>
void <%= class_name.capitalize%>TableModel::getFilter(eFilters filterType, string &filter, string column, string value1, string value2 /*=""*/)
{
	switch(filterType)
	{
		case eFilters::VARCHAR_START:
			filter += " like '" +  value1+ "%'";
			break;
		case eFilters::VARCHAR_CONTAINS:
		   filter += " like '%" +  value1+ "%'";
			break;
		case eFilters::VARCHAR_EQUALS:
			filter += " = '" +  value1+ "'";
			break;
		case eFilters::NUMBER_EQUALS:
		   filter += " = " +  value1;
			break;
		case eFilters::NUMBER_GREATER_EQUAL_THAN:
		   filter += " >= " +  value1;
			break;
		case eFilters::NUMBER_GREATER_THAN:
		   filter += " > " +  value1;
			break;
		case eFilters::NUMBER_LESS_EQUAL_THAN:
		   filter += " <= " +  value1;
			break;
		case eFilters::NUMBER_LESS_THAN:
		   filter += " < " +  value1;
			break;
		case eFilters::NUMBER_RANGE:
		   filter += " >= " +  value1  + " and "  + column+ " <= " + value2;
			break;
		case eFilters::DATE_GREATER_EQUAL_THAN:
		   filter += " >= " +  value1;
			break;
		case eFilters::DATE_GREATER_THAN:
		   filter += " > " +  value1;
			break;
		case eFilters::DATE_LESS_EQUAL_THAN:
		   filter += " <= " +  value1;
			break;
		case eFilters::DATE_LESS_THAN:
		   filter += " < " +  value1;
			break;
		case eFilters::DATE_RANGE:
		   filter += " between '" +  value1 + "' and '" + value2 + "'";
			break;
		case eFilters::NONE:
			break;
	}

}

