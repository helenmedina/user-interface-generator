#ifndef CONTROLLER_H
#define CONTROLLER_H
#include <<%= class_name.capitalize %>TableLogic.h>
#include <vector>

class QString;
class QTableWidget;

class Controller
{
public:
    Controller();
    QTableWidget* execute();
    QTableWidget* DisplayData(vector<vector<string>>& columnsOuput);
    MultiStringMap& GetFilters();
    MultiStringMap& GetOutputs();
    void DisableFilterWhenNoneSelected(QString name);

private:

    void SetOutputOptions();
    void SetFilterOptions();
    eFilters GetFilterType(QString filterType, QString type);
    string TreatConjunction(QString conjuntion);

    <%= class_name.capitalize %>TableLogic m_logic;
    QString m_tableName;

public:
    std::vector<st_ouputElements> m_outputElements;
    std::vector<st_filterElements*> m_filterElements;
};

#endif // CONTROLLER_H
