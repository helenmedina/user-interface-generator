#ifndef <%= class_name.upcase %>_TABLE_LOGIC_H
#define <%= class_name.upcase %>_TABLE_LOGIC_H

#include<vector>
#include "shared.h"
#include "<%= class_name.capitalize %>TableModel.h"


class QSqlDatabase;

struct st_columns
{
	<%- columnOutputDetails.each { |column| -%>
	<%= column[:columnType] %> <%= column[:columnName] %>;
	<%- } -%>
   //new colum for calculations
};

class <%= class_name.capitalize %>TableLogic
{

public:
	<%= class_name.capitalize %>TableLogic();   
	vector<vector<string>> execQuery();
	MultiStringMap& GetFiltersDefined();
	MultiStringMap& GetOutputsDefined();
	map<string, st_filterTabDetails&>& GetFilterTabDetails();
	void InsertOutput(string name, string option);
	void RemoveAllOutputMap();

private:
	void setFilter();   
	void getColumns(vector<string> &st);
	bool OpenConnection(QSqlDatabase &db);
	void SetUpFiltersDefined();
	void SetUpOutputsDefined();
	void InitFilters();
	<%-columnFilterDetails.each { |column| -%>
	void InitFilterColumn<%= column[:columnName].capitalize %>(); 
	<%- } -%>
	double Eval(double,double,string);
	<%-calculateDetails.each { |calculate| -%>
	double calculate_<%= calculate[:column] %>(double,double,string);
	<%- } -%>
	string getQuery();
	string treat<%= class_name.capitalize %>(string column, QString valueColumn);


	<%= class_name.capitalize %>TableModel m_model;
	<%- columnFilterDetails.each { |column| -%>
	st_filterTabDetails st_<%= column[:columnName] %>;
	<%- } -%>
	
	map<string, st_filterTabDetails&> m_filterDetails;
	map<string, string> m_outputOptions;
	MultiStringMap filters;
	MultiStringMap outputs;
	string m_query;

};

#endif // <%= class_name.upcase %>_TABLE_LOGIC_H
