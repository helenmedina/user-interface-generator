#include <QtSql>
#include "<%= class_name.capitalize %>TableLogic.h"
#include <string>
#include <map>

<%= class_name.capitalize %>TableLogic::<%= class_name.capitalize %>TableLogic()
{
	InitFilters();
	SetUpFiltersDefined();
	SetUpOutputsDefined();
}

void <%= class_name.capitalize %>TableLogic::setFilter()
{
	m_query = "select * from " + m_model.get_table_name() + " where ";

	map<string,st_filterTabDetails&>::iterator iter;

	for (iter = m_filterDetails.begin();  iter != m_filterDetails.end(); iter++)
	{
	<%-columnFilterDetails.each { |column| 
	if (column[:columnType] == "string")-%>
	if(iter->second.nameColumn == "<%= column[:columnName] %>" && iter->second.isEnabled)
	{
		m_query += m_model.get_filter_for_<%= column[:columnName] %>(st_<%= column[:columnName] %>.filterType, st_<%= column[:columnName] %>.value)
				+ " " +st_<%= column[:columnName] %>.conjunction + " ";
	}
	<%-else -%>
	if(iter->second.nameColumn == "<%= column[:columnName] %>" && iter->second.isEnabled)
	{
		m_query += m_model.get_filter_for_<%= column[:columnName] %>(st_<%= column[:columnName] %>.filterType, st_<%= column[:columnName] %>.value, st_<%= column[:columnName] %>.betweenValue)
				+ " " +st_<%= column[:columnName] %>.conjunction + " ";
	}
	<%-end } -%>
	}

}

vector<vector<string>> <%= class_name.capitalize %>TableLogic::execQuery()
{
	setFilter();
	QSqlDatabase db;
	vector<vector<string>> outputQuery;

	if (!OpenConnection(db))
		qDebug() << db.lastError();
	else
	{
		qDebug() << "Connected";

	QString str = QString::fromStdString(getQuery());
	QSqlQuery query(str );
	QSqlRecord record = query.record();


		while (query.next()) {


		<%-columnOutputDetails.each { |column| 
		if (column[:option] != "Calculate")-%>
			QString <%= column[:columnName] %> = query.value(record.indexOf("<%= column[:columnName] %>")).toString();
			<%- if (column[:columnType] == "int")-%>
			m_model.set_<%= column[:columnName] %>(atoi(treat<%= class_name.capitalize %>("<%= column[:columnName] %>", <%= column[:columnName] %>).c_str()));
			<%- elsif (column[:columnType] == "double")-%>
			m_model.set_<%= column[:columnName] %>(stof(treat<%= class_name.capitalize %>("<%= column[:columnName] %>", <%= column[:columnName] %>).c_str()));
			<%-else -%>
			m_model.set_<%= column[:columnName] %>(treat<%= class_name.capitalize %>("<%= column[:columnName] %>", <%= column[:columnName] %>));
			<%-end 
		end } -%>
		
		
		<%-calculateDetails.each { |calculate| -%>
			<%- if (calculate[:type1] == "column")-%>
			QString str_<%= calculate[:value1] %> = query.value(record.indexOf("<%= calculate[:value1] %>")).toString();
			double value_<%= calculate[:column] %>1 = str_<%= calculate[:value1] %>.toDouble();
			<%-else -%>
			double value_<%= calculate[:column] %>1 = <%= calculate[:value1] %>;
			<%-end -%>
			<%- if (calculate[:type2] == "column")-%>
			QString str_<%= calculate[:value2] %> = query.value(record.indexOf("<%= calculate[:value2] %>")).toString();
			double value_<%= calculate[:column] %>2 = str_<%= calculate[:value2] %>.toDouble();
			<%-else -%>
			double value_<%= calculate[:column] %>2 = <%= calculate[:value2] %>;
			<%-end -%>
			m_model.set_<%= calculate[:column] %>(calculate_<%= calculate[:column]%>(value_<%= calculate[:column] %>1, value_<%= calculate[:column] %>2,"<%= calculate[:operator] %>"));
		<%- } -%>

			vector<string> columns;
			getColumns(columns);
			outputQuery.push_back(columns);
		}
	}
	
	return outputQuery;
}

void <%= class_name.capitalize %>TableLogic::getColumns(vector<string> &columns)
{

	//columns.push_back(m_model.get_employee_name());

	<%-columnOutputDetails.each { |column| 
	if ((column[:columnType] == "int") || (column[:columnType] == "double"))-%>
	columns.push_back(std::to_string(m_model.get_<%= column[:columnName] %>()));
	<%-else -%>
	columns.push_back((m_model.get_<%= column[:columnName] %>()));
	<%-end } -%>
//10*employee_id
}

//output
string <%= class_name.capitalize %>TableLogic::treat<%= class_name.capitalize %>(string column, QString valueColumn)
{
	std::map<string, string>::iterator it;
	it = m_outputOptions.find(column);

	if(it != m_outputOptions.end())
	{
		string value= it->second;
		if (value == "lower case")
		{
		   valueColumn = valueColumn.toLower();
		}
		else if (value == "upper case")
		{
			valueColumn = valueColumn.toUpper();
		}
		else if (value == "yyyy-mm-dd")
		{
			valueColumn.toStdString();
		}
		else if (value == "yyyy/mm/dd")
		{
			valueColumn.replace("-","/").toStdString();
		}
	}
	return valueColumn.toStdString();

}

string <%= class_name.capitalize %>TableLogic::getQuery()
{
    return m_query;
}

//toUpper( str);
/***************************************************************/
bool <%= class_name.capitalize %>TableLogic::OpenConnection(QSqlDatabase &db)
/***************************************************************/
{
    bool bOpened = true;
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setDatabaseName("restaurant");
    db.setUserName("hmedina");
    db.setPassword("mysql");

    if (!db.open())
        bOpened = false;

    return bOpened;
}

void <%= class_name.capitalize %>TableLogic::SetUpFiltersDefined()
{
	stColumnDetails details;

	<% columnFilterDetails.each { |column| %>
	details.filterColumn = "None";
	details.typeColumn = "<%= column[:columnType] %>";
	filters.insert(pair<string,stColumnDetails>("<%= column[:columnName] %>",details));
	<% } %>

	<% filters.each { |filter| %>
	details.filterColumn = "<%= filter[:value] %>";
	details.typeColumn = "<%= filter[:type] %>";
	filters.insert(pair<string,stColumnDetails>("<%= filter[:column] %>",details));
	<% } %>

}

void <%= class_name.capitalize %>TableLogic::SetUpOutputsDefined()
{
	stColumnDetails details;
	<% columnOutputDetails.each { |column| %>
	details.filterColumn = "None";
	details.typeColumn = "<%= column[:columnType] %>";
	outputs.insert(pair<string,stColumnDetails>("<%= column[:columnName] %>",details));
	<% } %>
	
	<% outputs.each { |output| %>
	details.filterColumn = "<%= output[:value] %>";
	details.typeColumn = "<%= output[:type] %>";
	outputs.insert(pair<string,stColumnDetails>("<%= output[:column] %>",details));
	<% } %>

}

MultiStringMap& <%= class_name.capitalize %>TableLogic::GetFiltersDefined()
{
    return filters;
}

MultiStringMap& <%= class_name.capitalize %>TableLogic::GetOutputsDefined()
{
    return outputs;
}

map<string, st_filterTabDetails&>& <%= class_name.capitalize %>TableLogic::GetFilterTabDetails()
{
    return m_filterDetails;
}

void <%= class_name.capitalize %>TableLogic::InitFilters()
{
	<%-columnFilterDetails.each { |column| -%>
	InitFilterColumn<%= column[:columnName].capitalize %>(); 
	<%- } -%>

    //....asi con todas las columnas co filtros
}


<%-columnFilterDetails.each { |column| -%>
void <%= class_name.capitalize %>TableLogic::InitFilterColumn<%= column[:columnName].capitalize %>()
{
	st_<%= column[:columnName] %>.nameColumn = "<%= column[:columnName] %>";
	st_<%= column[:columnName] %>.conjunction = "";
	st_<%= column[:columnName] %>.value = "";
<%if not(column[:columnType] == "string")-%>
	st_<%= column[:columnName] %>.betweenValue = "";
<%-end -%>
	st_<%= column[:columnName] %>.isEnabled = false;
	m_filterDetails.insert ( std::pair<string,st_filterTabDetails&>("<%= column[:columnName] %>",st_<%= column[:columnName] %>) );
}
<%- } -%>

void <%= class_name.capitalize %>TableLogic::InsertOutput(string name, string option)
{

    m_outputOptions.insert(std::pair<string,string>(name,option) );

}

void <%= class_name.capitalize %>TableLogic::RemoveAllOutputMap()
{
    m_outputOptions.clear();
}


<%-calculateDetails.each { |calculate| -%>
double <%= class_name.capitalize %>TableLogic::calculate_<%= calculate[:column]%>(double value1, double value2, string _operator)
{
	double res = Eval(value1,value2,_operator);
	return res;
}
<%- } -%>

double <%= class_name.capitalize %>TableLogic::Eval(double value1, double value2, string _operator)
{
    double res = 0;
    if (_operator == "*")
        res = value1 * value2;
    else if (_operator == "+")
        res = value1 + value2;
    else if (_operator == "-")
        res = value1 - value2;
    else if (_operator == "/")
    {
        if (value2 != 0)
            res = value1/value2;
    }

    return res;
}
