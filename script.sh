#!/bin/bash

#Generate the output files
ruby Trabajo4.rb

if [ $? -ne 0 ]; then
	echo "Last operation failed"

else

	#Build the project
	cd ./output
	qmake
	make

	#Run the executable
	./QueryInterface

fi
