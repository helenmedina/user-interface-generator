require_relative 'XmlParser.rb'
require 'erb'


def CreateTableModelHeader(class_name, columnOutputDetails, columnFilterDetails)

	#create the ERB object to bind the arrays 
	erb = ERB.new( File.open( "./Templates/TemplateTableModel.h" ).read, nil, '-' )
	@ModelHeaderFile = erb.result( binding )

	#create the output file with the arrays and template information
	File.open( "./output/#{class_name.capitalize}TableModel.h", "w" ).write( @ModelHeaderFile )
	print "Creating #{class_name}\n"

end

def CreateTableModelCPP(class_name, columnOutputDetails, columnFilterDetails)

	#create the ERB object to bind the arrays 
	erb = ERB.new( File.open( "./Templates/TemplateTableModel.cpp" ).read, nil, '-' )
	@ModelHeaderCPP = erb.result( binding )

	#create the output file with the arrays and template information
	File.open( "./output/#{class_name.capitalize}TableModel.cpp", "w" ).write( @ModelHeaderCPP )
	print "Creating #{class_name}\n"

end

def CreateTableLogicHeader(class_name, calculateDetails, columnOutputDetails, columnFilterDetails)

	#create the ERB object to bind the arrays 
	erb = ERB.new( File.open( "./Templates/TemplateTableLogic.h" ).read, nil, '-' )
	@LogicHeaderFile = erb.result( binding )

	#create the output file with the arrays and template information
	File.open( "./output/#{class_name.capitalize}TableLogic.h", "w" ).write( @LogicHeaderFile )
	print "Creating #{class_name}\n"

end

def CreateTableLogicCPP(class_name, calculateDetails, filters, outputs, columnOutputDetails, columnFilterDetails)

	#create the ERB object to bind the arrays 
	erb = ERB.new( File.open( "./Templates/TemplateTableLogic.cpp" ).read, nil, '-' )
	@LogicHeaderCPP = erb.result( binding )

	#create the output file with the arrays and template information
	File.open( "./output/#{class_name.capitalize}TableLogic.cpp", "w" ).write( @LogicHeaderCPP )
	print "Creating #{class_name}\n"

end

def CreateControllerHeader(class_name)
	#create the ERB object to bind the arrays 
	erb = ERB.new( File.open( "./Templates/TemplateController.h" ).read, nil, '-' )
	@controller = erb.result( binding )

	#create the output file with the arrays and template information
	File.open( "./output/controller.h", "w" ).write( @controller )
	print "Creating controller.h\n"

end

def CreateTableViewCPP(class_name)
	#create the ERB object to bind the arrays 
	erb = ERB.new( File.open( "./Templates/TemplateTableView.cpp" ).read, nil, '-' )
	@tableView = erb.result( binding )

	#create the output file with the arrays and template information
	File.open( "./output/tableView.cpp", "w" ).write( @tableView )
	print "Creating tableView.cpp \n"

end

def CreateQueryInterface(class_name)
	#create the ERB object to bind the arrays 
	erb = ERB.new( File.open( "./Templates/TemplateQueryInterface.pro" ).read, nil, '-' )
	@queryInterface = erb.result( binding )

	#create the output file with the arrays and template information
	File.open( "./output/QueryInterface.pro", "w" ).write( @queryInterface )
	print "Creating QueryInterface.pro \n"

end

puts 'File name xml to parse: output.xml'
STDOUT.flush  
fileName = gets.chomp

if not fileName.match(/.xml\z/)
 fileName = fileName + '.xml'
 fileName = 'output.xml'
end


begin

	doc = REXML::Document.new( File.open( "#{fileName}" ) )
	#parse the input file
	parser = XmlParser.new
	parser.parse(doc)
	table = parser.GetTable
	filters = table.GetFilters
	outputs = table.GetOutputs
	columnOutputDetails = table.GetColumnOutputDetails
	columnFilterDetails = table.GetColumnFilterDetails
	calculateDetails = table.GetCalculateDetails
		
	class_name = "#{table.GetName()}"
	
	CreateTableModelHeader(class_name, columnOutputDetails, columnFilterDetails)
	CreateTableModelCPP(class_name, columnOutputDetails, columnFilterDetails)
	CreateTableLogicHeader(class_name, calculateDetails, columnOutputDetails, columnFilterDetails)
	CreateTableLogicCPP(class_name, calculateDetails, filters, outputs, columnOutputDetails, columnFilterDetails)
	CreateControllerHeader(class_name)
	CreateTableViewCPP(class_name)
	CreateQueryInterface(class_name)


rescue REXML::ParseException => e
	puts "Failed when parsing document: #{e.message}"
rescue RuntimeError => e
	puts "Failed: #{e.message}"
rescue 
	puts "Failed: #{$!.message}" 
end

